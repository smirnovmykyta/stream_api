package com.gmail.mykyta.smirnov;

import java.util.List;
import java.util.stream.Collectors;

public class ProcessingStudent {

    public static List<Student> getListOfStudentsByFaculty(List<Student> stundets,  String faculty){
        return stundets.stream()
                .filter((student) -> student.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    public static List<Student> getListOfStudensByFacultyAndCourse(List<Student> students, String faculty, String course){
        return students.stream()
                .filter((student) -> student.getFaculty().equals(faculty) && student.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    public static List<Student> getListOfStudentsBornAfterGivenYear(List<Student> students, int year){
        return students.stream()
                .filter((student) -> student.getYearOfBirth() >= year)
                .collect(Collectors.toList());
    }

    public static List<String> getListOfStudentsInGroup(List<Student> students, String group){
        List<String> listStudentsInGroup = students.stream()
                .filter((student) -> student.getGroup().equals(group))//Через ", ", так как в дальнейшем може понадобится сплитить
                .map(student -> student.getLastName() + ", "
                        + student.getFirstName() + ", "
                        + student.getPatronymic())
                .collect(Collectors.toList());

        return listStudentsInGroup;
    }

    public static List<String> displayListOfStudents(List<Student> students){

        List<String> listStudents = students.stream()
                .map(student -> student.getLastName() + ", "
                        + student.getFirstName() + ", "
                        + student.getPatronymic() + ", "
                        + student.getFaculty() + ", "
                        + student.getGroup())
                .peek(System.out :: println)
                .collect(Collectors.toList());

        return listStudents;
    }

    public static long countStudentsInFaculty(List<Student> students, String faculty){
        return students.stream()
                .filter((student) -> student.getFaculty().equals(faculty))
                .count();
    }
}
