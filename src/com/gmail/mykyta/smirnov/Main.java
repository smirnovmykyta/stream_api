package com.gmail.mykyta.smirnov;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();

        students.add(new Student(001, "Nikita", "Smirnov", "Bogdanovich",
                1998, "Kyiv", "856584265", "Project managment", "VI", "PM(KN)-21"));
        students.add(new Student(002, "Vasya", "Pupkin", "Popkovich",
                1994, "Lviv", "0455466456", "Tam", "IV", "AM-52"));
        students.add(new Student(003, "Nikita", "Tsikalenko", "Igorovich",
                1997, "Kyiv", "0672891086", "Project managment", "VI", "PM(KN)-21"));
        students.add(new Student(004, "Vania", "Solomka", "Gennadiiovych",
                1993, "Kyiv", "086235", "Medical", "V", "DBD"));
        students.add(new Student(005, "Sanya", "Ti", "Vpotyadke",
                1995, "Dnipro", "86356559", "Medical", "V", "DMD"));
        students.add(new Student(006, "Serhii", "Kichuk", "Vitalievich",
                1998, "Kyiv", "54464655486", "Managment", "VI", "PM(KN)-21"));
        students.add(new Student(007, "Petr", "Vodkin", "Vodkovich",
                1992, "Kyiv", "123456789", "Tam", "III", "AM-52"));
//        System.out.println(ProcessingStudent.getListOfStudentsByFaculty(students, "Project managment"));
//        System.out.println(ProcessingStudent.getListOfStudensByFacultyAndCourse(students, "Project managment", "VI"));
//        System.out.println(ProcessingStudent.getListOfStudentsBornAfterGivenYear(students, 1998));
//        System.out.println(ProcessingStudent.getListOfStudentsInGroup(students, "PM(KN)-21"));
        ProcessingStudent.displayListOfStudents(students);
//        System.out.println(ProcessingStudent.countStudentsInFaculty(students, "AIT"));
    }
}
