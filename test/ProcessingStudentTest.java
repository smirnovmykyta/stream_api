import com.gmail.mykyta.smirnov.ProcessingStudent;
import com.gmail.mykyta.smirnov.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ProcessingStudentTest {

    ArrayList<Student> test = new ArrayList<>();

    @BeforeEach
    public void createTestArray() {
        test.add(new Student(001, "Nikita", "Smirnov", "Bogdanovich",
                1998, "Kyiv", "856584265", "Project managment", "VI", "PM(KN)-21"));
        test.add(new Student(002, "Vasya", "Pupkin", "Popkovich",
                1994, "Lviv", "0455466456", "Tam", "IV", "AM-52"));
        test.add(new Student(003, "Nikita", "Tsikalenko", "Igorovich",
                1997, "Kyiv", "0672891086", "Project managment", "VI", "PM(KN)-21"));
        test.add(new Student(004, "Vania", "Solomka", "Gennadiiovych",
                1993, "Kyiv", "086235", "Medical", "V", "DBD"));
        test.add(new Student(005, "Sanya", "Ti", "Vpotyadke",
                1995, "Dnipro", "86356559", "Medical", "V", "DMD"));
        test.add(new Student(006, "Serhii", "Kichuk", "Vitalievich",
                1998, "Kyiv", "54464655486", "Managment", "VI", "PM(KN)-21"));
        test.add(new Student(007, "Petr", "Vodkin", "Vodkovich",
                1992, "Kyiv", "123456789", "Tam", "III", "AM-52"));
    }

    @Test
    public void shouldGetStudentsOnFaculty() {

        //given
        String faculty = "Project managment";

        //when
        String expected = "[Student{id=1, firstName='Nikita', lastName='Smirnov', patronymic='Bogdanovich', yearOfBirth=1998, address='Kyiv', telephone='856584265', faculty='Project managment', course='VI', group='PM(KN)-21'}, Student{id=3, firstName='Nikita', lastName='Tsikalenko', patronymic='Igorovich', yearOfBirth=1997, address='Kyiv', telephone='0672891086', faculty='Project managment', course='VI', group='PM(KN)-21'}]";
        List<Student> actual = ProcessingStudent.getListOfStudentsByFaculty(test, faculty);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsOnFacultyAndCourse() {

        //given
        String faculty = "Project managment";
        String course = "VI";

        //when
        String expected = "[Student{id=1, firstName='Nikita', lastName='Smirnov', patronymic='Bogdanovich', yearOfBirth=1998, address='Kyiv', telephone='856584265', faculty='Project managment', course='VI', group='PM(KN)-21'}, Student{id=3, firstName='Nikita', lastName='Tsikalenko', patronymic='Igorovich', yearOfBirth=1997, address='Kyiv', telephone='0672891086', faculty='Project managment', course='VI', group='PM(KN)-21'}]";
        List<Student> actual = ProcessingStudent. getListOfStudensByFacultyAndCourse(test, faculty, course);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsAfterYearOfBirth() {

        //given
        int yearOfBirth = 1998;

        //when
        String expected = "[Student{id=1, firstName='Nikita', lastName='Smirnov', patronymic='Bogdanovich', yearOfBirth=1998, address='Kyiv', telephone='856584265', faculty='Project managment', course='VI', group='PM(KN)-21'}, Student{id=6, firstName='Serhii', lastName='Kichuk', patronymic='Vitalievich', yearOfBirth=1998, address='Kyiv', telephone='54464655486', faculty='Managment', course='VI', group='PM(KN)-21'}]";
        List<Student> actual = ProcessingStudent.getListOfStudentsBornAfterGivenYear(test, yearOfBirth);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsInGroup() {

        //given
        String group = "PM(KN)-21";

        //when
        String expected = "[Smirnov, Nikita, Bogdanovich, Tsikalenko, Nikita, Igorovich, Kichuk, Serhii, Vitalievich]";
        List<String> actual = ProcessingStudent.getListOfStudentsInGroup(test, group);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldOutputListOfStudents() {

        //given

        //when
        String expected = "[Smirnov, Nikita, Bogdanovich, Project managment, PM(KN)-21, Pupkin, Vasya, Popkovich, Tam, AM-52, Tsikalenko, Nikita, Igorovich, Project managment, PM(KN)-21, Solomka, Vania, Gennadiiovych, Medical, DBD, Ti, Sanya, Vpotyadke, Medical, DMD, Kichuk, Serhii, Vitalievich, Managment, PM(KN)-21, Vodkin, Petr, Vodkovich, Tam, AM-52]";
        List<String> actual = ProcessingStudent.displayListOfStudents(test);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldCountStudentsOnFaculty() {

        //given
        String faculty = "Project managment";

        //when
        long expected = 2;
        long actual = ProcessingStudent.countStudentsInFaculty(test, faculty);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }
}
